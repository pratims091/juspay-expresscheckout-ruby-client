require 'expresscheckout/errors'
require 'expresscheckout/version'
require 'unirest'
require 'socket'

module Expresscheckout
  class Utils

    def self.request(method, url, parameters={})
      begin
        if $expresscheckout_environment == 'production'
          $server = 'https://api.juspay.in'
        elsif $expresscheckout_environment == 'staging'
          $server = 'https://sandbox.juspay.in'
        else
          raise InvalidArguementError.new('ERROR: environment variable can be "production" or "staging"')
        end
        if $expresscheckout_api_key == nil
          raise AuthenticationError.new("ERROR: API key missing. Please specify api_key.")
        end
        $headers = {
          'version' => $api_version,
          'User-Agent' => "Ruby SDK #{Expresscheckout::VERSION}"
        }
        if method == 'GET'
          response = Unirest.get $server+url, headers: $headers, auth: {:user => $expresscheckout_api_key, :password => ''}, parameters: parameters
        else
          response = Unirest.post $server +url, headers: $headers, auth: {:user => $expresscheckout_api_key, :password => ''}, parameters: parameters
        end
        if (response.code >= 200 && response.code < 300)
          return response
        elsif ([400, 404].include? response.code)
          raise InvalidRequestError.new('Invalid Request', response.code, response.body, parameters)
        elsif (response.code == 401)
          raise AuthenticationError.new('Unauthenticated Request', response.code, response.body, parameters)
        else
          raise APIError.new('Invalid Request', response.code, response.body, parameters)
        end
      rescue IOError
        raise APIConnectionError.new('Connection error')
      rescue SocketError
        raise APIConnectionError.new('Socket error. Failed to connect.')
      end
    end

    def self.get_arg(options = {}, param)
      if options == NIL
        NIL
      elsif options.key?(param)
        options[param]
      else
        NIL
      end
    end

    def self.check_param(options = {}, param)
      options.each do |key, _|
        if key.include?(param)
          return true
        end
      end
      false
    end
  end

end
