require 'expresscheckout/utils'

module Expresscheckout

  class Wallets

    # noinspection ALL
    class Wallet

      attr_reader :id, :wallet, :object, :token, :linked, :current_balance, :last_refresh

      def initialize(options = {})
        @id = Utils.get_arg(options, 'id')
        @object = Utils.get_arg(options, 'object')
        @wallet = Utils.get_arg(options, 'wallet')
        @token = Utils.get_arg(options, 'token')
        @linked = Utils.get_arg(options, 'linked')
        @current_balance = Utils.get_arg(options, 'current_balance')
        @last_refresh = Utils.get_arg(options, 'last_refreshed')
      end

    end

    def Wallets.create(options={})
      customer_id = Utils.get_arg(options, :customer_id)
      gateway = Utils.get_arg(options, :gateway)

      if customer_id == NIL or customer_id == '' or gateway == NIL or gateway == ''
        raise InvalidArguementError.new("ERROR: `customer_id` and `gateway` are required parameters for Wallets.create()")
      end

      url = "/customers/#{customer_id}/wallets"

      method = 'POST'
      parameters = {
        :gateway => gateway
      }
      response = Utils.request(method,url,parameters)
      wallet = Wallet.new(response.body)
      return wallet
    end

    def Wallets.create_and_authenticate(options={})
      customer_id = Utils.get_arg(options, :customer_id)
      gateway = Utils.get_arg(options, :gateway)

      if customer_id == NIL or customer_id == '' or gateway == NIL or gateway == ''
        raise InvalidArguementError.new("ERROR: `customer_id` and `gateway` are required parameters for Wallets.create_and_authenticate()")
      end

      url = "/customers/#{customer_id}/wallets"

      method = 'POST'
      parameters = {
        :command => 'authenticate',
        :gateway => gateway
      }
      response = Utils.request(method,url,parameters)
      wallet = Wallet.new(response.body)
      return wallet
    end

    def Wallets.list(options={})
      order_id = Utils.get_arg(options, :order_id)
      customer_id = Utils.get_arg(options, :customer_id)

      if (customer_id == NIL or customer_id == '') and (order_id == NIL or order_id == '')
        raise InvalidArguementError.new("ERROR: `customer_id` or `order_id` is required parameter for Wallets.list()")
      end

      if customer_id
        url = "/customers/#{customer_id}/wallets"
      else
        url = "/orders/#{order_id}/wallets"
      end

      method = 'GET'
      response = Array(Utils.request(method,url,{}).body['list'])
      wallets = []
      i=0
      while i != response.count
        wallet = Wallet.new(response[i])
        wallets.push(wallet)
        i+=1
      end
      return wallets
    end

    def Wallets.refresh_balance(options={})
      customer_id = Utils.get_arg(options, :customer_id)

      if customer_id == NIL or customer_id == ''
        raise InvalidArguementError.new("ERROR: `customer_id` is required parameter for Wallets.refresh_balance()")
      end

      url = "/customers/#{customer_id}/wallets/refresh-balances"
      method = 'GET'
      response = Array(Utils.request(method,url,{}).body['list'])
      wallets = []
      i=0
      while i != response.count
        wallet = Wallet.new(response[i])
        wallets.push(wallet)
        i+=1
      end
      return wallets
    end

    def Wallets.refresh_by_wallet_id(options={})
      wallet_id = Utils.get_arg(options, :wallet_id)

      if wallet_id == NIL or wallet_id == ''
        raise InvalidArguementError.new("ERROR: `wallet_id` is required parameter for Wallets.refresh_by_wallet_id()")
      end

      url = "/wallets/#{wallet_id}"

      method = 'GET'
      parameters = {
        :command => 'refresh'
      }
      response = Utils.request(method,url,parameters)
      wallet = Wallet.new(response.body)
      return wallet
    end


    def Wallets.authenticate(options={})
      wallet_id = Utils.get_arg(options, :wallet_id)

      if wallet_id == NIL or wallet_id == ''
        raise InvalidArguementError.new("ERROR: `wallet_id` is required parameter for Wallets.authenticate()")
      end

      url = "/wallets/#{wallet_id}"

      method = 'POST'
      parameters = {
        :command => 'authenticate'
      }
      response = Utils.request(method,url,parameters)
      wallet = Wallet.new(response.body)
      return wallet
    end

    def Wallets.link(options={})
      wallet_id = Utils.get_arg(options, :wallet_id)
      otp = Utils.get_arg(options, :otp)

      if wallet_id == NIL or wallet_id == '' or otp == NIL or otp == ''
        raise InvalidArguementError.new("ERROR: `wallet_id` and `otp` are required parameters for Wallets.link()")
      end

      url = "/wallets/#{wallet_id}"

      method = 'POST'
      parameters = {
        :command => 'link',
        :otp => otp
      }
      response = Utils.request(method,url,parameters)
      wallet = Wallet.new(response.body)
      return wallet
    end

    def Wallets.delink(options={})
      wallet_id = Utils.get_arg(options, :wallet_id)

      if wallet_id == NIL or wallet_id == ''
        raise InvalidArguementError.new("ERROR: `wallet_id` is required parameter for Wallets.delink()")
      end

      url = "/wallets/#{wallet_id}"

      method = 'POST'
      parameters = {
        :command => 'delink'
      }
      response = Utils.request(method,url,parameters)
      wallet = Wallet.new(response.body)
      return wallet
    end
  end
end
