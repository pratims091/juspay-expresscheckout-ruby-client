require 'expresscheckout/orders'
require 'expresscheckout/cards'
require 'expresscheckout/payments'
require 'expresscheckout/wallets'
require 'expresscheckout/customers'
require 'minitest/autorun'
require 'pp'

def pp(obj)
  text = obj.pretty_inspect
  output = ''
  text.each_line do |line|
    unless line.include?('=nil') or line.include?('=""')
      output += line
    end
  end
  puts output
end

class Test < Minitest::Test
  $expresscheckout_api_key = '187BF8D543A545789497F14FCAFBD85C'
  $expresscheckout_environment = 'staging'

  def setup
    @timestamp = Time.now.to_i
  end

  def test_orders
    # Test for create
    order = Expresscheckout::Orders.create(order_id:@timestamp, amount:1000)
    assert_equal('CREATED', order.status)
    # Test for payment links
    refute_nil(order.payment_links)

    #pp order

    # Test for get_status
    status = Expresscheckout::Orders.status(order_id:@timestamp)
    assert_equal(Float(status.order_id), @timestamp)
    #pp status

    # Test for list
    order_list = Expresscheckout::Orders.list
    refute_nil(order_list)
    #pp order_list

    # Test for update
    updated_order = Expresscheckout::Orders.update(order_id:@timestamp, amount:500)
    status = Expresscheckout::Orders.status(order_id:@timestamp)
    assert_equal(status.amount, updated_order.amount)
  end

  def test_refund
    # Test for refund
    refunded_order = Expresscheckout::Orders.refund(unique_request_id: @timestamp, order_id: '1471441349',amount: 1)
    #pp refunded_order
  end

  def self.delete_all_cards
    card_list = Expresscheckout::Cards.list(:customer_id=>'user')
    card_list.each do |card|
      Expresscheckout::Cards.delete(:card_token=>card.token)
    end
  end

  def test_cards

    # Test for add
    card = Expresscheckout::Cards.create(merchant_id:'shreyas', customer_id:'user', customer_email:'abc@xyz.com',
        card_number:@timestamp*(10**6), card_exp_year:'20', card_exp_month:'12')
    refute_nil(card.reference)
    refute_nil(card.token)
    refute_nil(card.fingerprint)
    #pp card
    # Test for delete
    deleted_card = Expresscheckout::Cards.delete(card_token:card.token)
    assert(true,deleted_card.deleted)
    #pp deleted_card
    # Test for list
    #Test.delete_all_cards
    Expresscheckout::Cards.create(merchant_id:'shreyas', customer_id:'user', customer_email:'abc@xyz.com',
        card_number:@timestamp * (10 ** 6), card_exp_year:'20', card_exp_month:'12')
    Expresscheckout::Cards.create(merchant_id:'shreyas', customer_id:'user', customer_email:'abc@xyz.com',
        card_number:@timestamp * (10 ** 6)+1, card_exp_year:'20', card_exp_month:'12')
    card_list = Expresscheckout::Cards.list(customer_id:'user')
    refute_nil(card_list)
    assert_equal(card_list.length, 2)
    #pp card_list
    Test.delete_all_cards
  end

  def test_payments

    # Test for create_card_payment
    payment = Expresscheckout::Payments.create_card_payment(
        order_id:1470043679,
        merchant_id:'azhar',
        redirect_after_payment:false,
        card_number:'4242424242424242',
        name_on_card:'Customer',
        card_exp_year:'20',
        card_exp_month:'12',
        card_security_code:'123',
        save_to_locker:false)
    refute_nil(payment.txn_id)
    assert_equal(payment.status, 'PENDING_VBV')

    # Test for create_net_banking_payment
    payment = Expresscheckout::Payments.create_net_banking_payment(
        order_id:1470043679,
        merchant_id:'azhar',
        payment_method:'NB_DUMMY',
        redirect_after_payment:false)
    refute_nil(payment.txn_id)
    assert_equal(payment.status, 'PENDING_VBV')

    # Test for create_wallet_payment
    payment = Expresscheckout::Payments.create_wallet_payment(
        order_id:'check5',
        merchant_id:'azharamin',
        payment_method:'MOBIKWIK',
        redirect_after_payment:false)
    refute_nil(payment.txn_id)
    assert_match(/^(PENDING_VBV|CHARGED)$/,payment.status)
  end

  def test_customers
    #Add customer
    customer = Expresscheckout::Customers.create(
        object_reference_id: "CustId#{@timestamp}",
        mobile_number: '7272727272',
        email_address: 'az@temp.com',
        first_name: 'temp',
        last_name: 'kumar',
        mobile_country_code: '35'
      )
    refute_nil(customer)

    #Get customer
    customer = Expresscheckout::Customers.get(
        customer_id: customer.id
    )
    refute_nil(customer)

    #Update customer
    customer = Expresscheckout::Customers.update(
        customer_id: customer.id,
        mobile_number: '7272727273',
        email_address: 'newaz@temp.com',
        first_name: 'newtemp',
        last_name: 'newkumar',
        mobile_country_code: '91'
    )
    refute_nil(customer)

    #List customer
    customer = Expresscheckout::Customers.list(
        count: 1
    )
    assert_equal(customer['count'], 1)
  end

  def test_wallets

    # List wallets by orderId Deprecated
    # wallets = Expresscheckout::Wallets.list(order_id:'1471434419')
    # refute_nil(wallets[0].id)

    # List wallets by customerId
    wallets = Expresscheckout::Wallets.list(customer_id: 'guest_user_101')
    refute_nil(wallets[0].id)

    # List wallets by customerId
    wallets = Expresscheckout::Wallets.refresh_balance(customer_id:'guest_user_104')
    refute_nil(wallets[0].id)
  end
end

