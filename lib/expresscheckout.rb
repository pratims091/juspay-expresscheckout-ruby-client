require "expresscheckout/version"

module Expresscheckout
  require_relative 'expresscheckout/orders'
  require_relative 'expresscheckout/cards'
  require_relative 'expresscheckout/payments'
  require_relative 'expresscheckout/customers'
  require_relative 'expresscheckout/wallets'

  $api_key = nil
  $environment = 'production'
  $api_version= '2016-10-27'
end

require 'unirest'
